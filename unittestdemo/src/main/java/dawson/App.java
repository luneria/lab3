package dawson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    // Number 3
    public int echo(int x)
    {
        return x;
    }

    // Number 5
    public int oneMore(int x)
    {
        return x+1-1;
    }
}
