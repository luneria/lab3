package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    // Number 4
    @Test
    public void testEcho()
    {
        App test = new App();
        assertEquals("Test echo method", 5, test.echo(5));
    }

    // Number 6
    @Test
    public void testOneMore()
    {
        App test2 = new App();
        assertEquals("Test oneMore method", 6, test2.oneMore(5));
    }

}
